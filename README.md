# Device tree for Xiaomi Redmi Note 3 and Xiaomi Redmi Note 3 Special Edition (codenamed _"kenzo/kate"_)

Kernel source: "https://github.com/AmolAmrit/Escrima_kernel_xiaomi_msm8956.git"
==================================
## Device specifications

| Feature                 | Specification                         |
| :---------------------- | :------------------------------------ |
| CPU                     | Quad-core 1.4 GHz Cortex-A53 & Dual-core 1.8 GHz Cortex-A72 |
| Chipset                 | Qualcomm MSM8956 Snapdragon 650       |
| GPU                     | Adreno 510                            |
| Memory                  | 2GB/3GB DDR3 Dual-channel             |
| Shipped Android Version | 7.1.1                                 |
| Storage                 | 16GB/32GB                             |
| MicroSD                 | Up to 32GB                            |
| Battery                 | Non-removable Li-Po 4050 mAh battery  |
| Display                 | 1920x1080 pixels, 5.5 (~401 PPI)      |
| Camera                  | Primary: 16 MP, f/2.0, dual-LED flash |
| Camera	          | Secondary: 5 MP, f/2.0, 1080p         |

## Device picture

![Xiaomi Redmi Note 3](http://cdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-note-3-1.jpg "Xiaomi Redmi Note 3")

### Copyright
 ```
  /*
  *  Copyright (C) 2013-2020 The TWRP
  *
  *  Copyright (C) 2019-2020 The OrangeFox Recovery Project
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  */
  ```
